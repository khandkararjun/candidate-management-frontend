import { TestBed } from '@angular/core/testing';

import { DetailedGradService } from './detailed-grad.service';

describe('DetailedGradService', () => {
  let service: DetailedGradService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetailedGradService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
