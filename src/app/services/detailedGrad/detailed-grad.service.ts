import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subscribable, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DetailedGradService 
{
  url = 'http://localhost:9000/grad-management/grad/';

  constructor(private http: HttpClient) {}

  getGradById(id: number): Observable<any> {
    const headers = new HttpHeaders({
     'idToken': localStorage.getItem('idToken')
    });
    return this.http.get(this.url+id, {observe: 'response', headers: headers});
  }

}
