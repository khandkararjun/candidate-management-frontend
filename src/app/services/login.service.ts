import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/User';
import { SocialUser } from 'angularx-social-login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {}

  addUser(socialUser: SocialUser) {
    let user = new User(socialUser.email, socialUser.authToken, socialUser.name);
    let headers = new HttpHeaders({
      'idToken': socialUser.idToken
    });
    
    return this.http.post("http://localhost:9000/login", user, { headers: headers});
  }
}
