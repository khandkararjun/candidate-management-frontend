import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GradHistoryService 
{
  constructor(private http: HttpClient) {}

  public getGradHistory(id: number): Observable<HttpResponse<any>> {
    let headers = new HttpHeaders({
      'idToken': localStorage.getItem('idToken')
    });
    const url = 'http://localhost:9000/grad-management/grad/';
    return this.http.get(url+id+'/history', {observe: 'response', headers: headers});
  }
}
