import { TestBed } from '@angular/core/testing';

import { GradHistoryService } from './grad-history.service';

describe('GradHistoryService', () => {
  let service: GradHistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GradHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
