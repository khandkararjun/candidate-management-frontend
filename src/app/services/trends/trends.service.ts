import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TrendsService 
{
  constructor(private http: HttpClient) {}

  public getLocationTrends(): Observable<HttpResponse<any>>{
    const url = 'http://localhost:9000/grad-management/location-trends';
    const headers = new HttpHeaders({
      'idToken': localStorage.getItem('idToken')
    });
    return this.http.get(url, {headers: headers, observe: 'response'});
  }
}
