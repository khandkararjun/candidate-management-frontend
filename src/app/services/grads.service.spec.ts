import { TestBed } from '@angular/core/testing';

import { GradsService } from './grads.service';

describe('GradsService', () => {
  let service: GradsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GradsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
