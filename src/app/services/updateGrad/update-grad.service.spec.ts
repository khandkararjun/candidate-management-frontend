import { TestBed } from '@angular/core/testing';

import { UpdateGradService } from './update-grad.service';

describe('UpdateGradService', () => {
  let service: UpdateGradService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UpdateGradService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
