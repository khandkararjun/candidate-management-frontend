import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Grad } from 'src/app/models/Grad';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UpdateGradService 
{
  private url = 'http://localhost:9000/grad-management/update-grad';

  constructor(private http: HttpClient) {}

  public updateGrad(grad: Grad): Observable<any> {
    return this.http.post(this.url, grad);
  }
}
