import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GuardService {

  constructor(private router: Router) {}

  isAuthorized() {
    return !!localStorage.getItem('verifiedUser');
  }

  canActivate(): Boolean {
    if(!this.isAuthorized()) {
      this.router.navigate(['login']);
      return false;
    } else {
      return true;
    }
  }
}
