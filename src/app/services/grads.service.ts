import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GradsService 
{
  constructor(private http: HttpClient) {}

  public getGrads(): Observable<any> {
    let headers = new HttpHeaders({
      'idToken': localStorage.getItem('idToken')
    });
    const url = 'http://localhost:9000/grad-management/grads';
    return this.http.get(url, {headers: headers});
  }

}
