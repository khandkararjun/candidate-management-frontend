import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SkillsService 
{
  url = "http://localhost:9000/skill-management/skills";

  constructor(private http: HttpClient) {}

  getSkills(): Observable<HttpResponse<any>> {
    return this.http.get(this.url, {observe: 'response'});
  }
}
