import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeleteGradService 
{
  url = "http://localhost:9000/grad-management/delete-grad/"

  constructor(private http: HttpClient) {}

  deleteGrad(id: number): Observable<any> {
    console.log("Calling: "+this.url+id);
    return this.http.delete(this.url+id);
  }
}
