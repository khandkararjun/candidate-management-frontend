import { TestBed } from '@angular/core/testing';

import { DeleteGradService } from './delete-grad.service';

describe('DeleteGradService', () => {
  let service: DeleteGradService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeleteGradService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
