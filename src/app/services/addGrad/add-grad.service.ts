import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Grad } from '../../models/Grad';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddGradService 
{
  private url = 'http://localhost:9000/grad-management/add-grad';

  constructor(private http: HttpClient) {}

  public addGrad(grad: Grad): Observable<any> {
    const headers = new HttpHeaders({
      'idToken': localStorage.getItem('idToken')
    });
    return this.http.post(this.url, grad, {headers: headers});
  }
}
