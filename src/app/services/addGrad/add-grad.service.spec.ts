import { TestBed } from '@angular/core/testing';

import { AddGradService } from './add-grad.service';

describe('AddGradService', () => {
  let service: AddGradService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddGradService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
