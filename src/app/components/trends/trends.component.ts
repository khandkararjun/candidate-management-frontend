import { Component, OnInit } from '@angular/core';

import { TrendsService } from '../../services/trends/trends.service';

import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.css']
})
export class TrendsComponent implements OnInit {

  public pieChartOptions: ChartOptions = {
    responsive: true
  };

  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors: Array < any > = [{
    backgroundColor: ['rgb(185,0,201)', 'rgb(0,201,114)', 'rgb(0,121,201)', 
      'rgb(201,50,0)', 'rgb(168,201,0)', 'rgb(0,201,37)', 'rgb(201,0,111)', 'rgb(201,0,0)'],
 }];

  constructor(private trends: TrendsService) {
    monkeyPatchChartJsLegend();
    monkeyPatchChartJsTooltip();
  }

  ngOnInit(): void {
    this.trends.getLocationTrends().subscribe((resp) => {
      console.log(resp.body.responseObject);

      for(const res of resp.body.responseObject) {
        this.pieChartLabels.push(res.town);
        this.pieChartData.push(res.frequency);
      }
    });
  }

}
