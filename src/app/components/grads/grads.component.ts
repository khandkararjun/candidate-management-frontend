import { Component, OnInit } from '@angular/core';
import { GradsService } from '../../services/grads.service';

import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-grads',
  templateUrl: './grads.component.html',
  styleUrls: ['./grads.component.css']
})
export class GradsComponent implements OnInit {

  public grads;

  public columnsToDisplay = ['id', 'name', 'gender', 'email', 'contact', 'town', 'eduSummary', 'more'];

  constructor(private gs: GradsService) { }

  ngOnInit(): void {
    this.gs.getGrads().subscribe((grads) => {
      this.grads = new MatTableDataSource(grads.responseObject);
    })
  }

}
