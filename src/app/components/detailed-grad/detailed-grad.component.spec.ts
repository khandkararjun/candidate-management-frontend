import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedGradComponent } from './detailed-grad.component';

describe('DetailedGradComponent', () => {
  let component: DetailedGradComponent;
  let fixture: ComponentFixture<DetailedGradComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedGradComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedGradComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
