import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DeleteGradService } from '../../services/deleteGrad/delete-grad.service';
import { DetailedGradService } from '../../services/detailedGrad/detailed-grad.service';
import { Grad } from 'src/app/models/Grad';
import { isBuffer } from 'util';

@Component({
  selector: 'app-detailed-grad',
  templateUrl: './detailed-grad.component.html',
  styleUrls: ['./detailed-grad.component.css']
})
export class DetailedGradComponent implements OnInit 
{
  gradId: number;
  public grad: Grad;

  constructor(
      public r: Router, 
      public ar: ActivatedRoute, 
      private dgs: DetailedGradService, 
      public delGs: DeleteGradService) 
  {
    this.ar.params.subscribe((params) => {this.gradId = params.id;});
    this.dgs.getGradById(this.gradId).subscribe((resp) => {
    
      console.log(resp.body);
     
      if(resp.status == 401) {
        localStorage.clear();
        this.r.navigateByUrl("/login");
      } else if(resp.status == 204) {
        this.r.navigateByUrl("/oops");
      } else {
        this.grad = resp.body.responseObject;        
      }
    });
  }

  deleteGrad(id: number) {
    this.delGs.deleteGrad(id).subscribe((resp) => {
      console.log(resp);
    });
  }

  ngOnInit(): void {
    console.log(this.gradId);
  }
}
