import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SkillsService } from '../../services/skills/skills-service.service';
import { DetailedGradService } from '../../services/detailedGrad/detailed-grad.service';
import { Grad } from '../../models/Grad';

import { UpdateGradService } from '../../services/updateGrad/update-grad.service';

@Component({
  selector: 'app-edit-grad',
  templateUrl: './edit-grad.component.html',
  styleUrls: ['./edit-grad.component.css']
})
export class EditGradComponent implements OnInit 
{
  gradId: number;
  public grad: Grad;
  addGradForm: FormGroup;

  public skillsList;

  constructor(private r: Router, private ar: ActivatedRoute, private dgs: DetailedGradService, private ss: SkillsService, private ugs: UpdateGradService) {
    this.ar.params.subscribe((param) => {this.gradId = param.id});
    
    
      this.addGradForm = new FormGroup({
        id: new FormControl(''),
        creator: new FormControl(''),
        lastUpdateBy: new FormControl(localStorage.getItem('verifiedUser')),
        createDate: new FormControl(''),
        updateDate: new FormControl(''),
        fname: new FormControl(''),
        mname: new FormControl(''),
        lname: new FormControl(''),
        gender: new FormControl(''),
        email: new FormControl(''),
        contact: new FormControl(''),
        addressLn1: new FormControl(''),
        town: new FormControl(''),
        state: new FormControl(''),
        pinCode: new FormControl(''),
        institute: new FormControl(''),
        degree: new FormControl(''),
        branch: new FormControl(''),
        joinDate: new FormControl(''),
        joinLoc: new FormControl(''),
        feedback: new FormControl(''),
        skills: new FormControl('')
      });

    ss.getSkills().subscribe((resp) => {
      this.skillsList = resp.body.responseObject;
    });

    this.dgs.getGradById(this.gradId).subscribe((resp) => {

      if(resp.status != 200) {
        r.navigateByUrl("/oops");
      } else {
        this.grad = resp.body.responseObject;

      this.addGradForm = new FormGroup({
        id: new FormControl(this.grad.id),
        creator: new FormControl(this.grad.creator),
        lastUpdateBy: new FormControl(localStorage.getItem('verifiedUser')),
        createDate: new FormControl(this.grad.createDate),
        updateDate: new FormControl(''),
        fname: new FormControl(this.grad.fname),
        mname: new FormControl(this.grad.mname),
        lname: new FormControl(this.grad.lname),
        gender: new FormControl(this.grad.gender),
        email: new FormControl(this.grad.email),
        contact: new FormControl(this.grad.contact),
        addressLn1: new FormControl(this.grad.addressLn1),
        town: new FormControl(this.grad.town),
        state: new FormControl(this.grad.state),
        pinCode: new FormControl(this.grad.pinCode),
        institute: new FormControl(this.grad.institute),
        degree: new FormControl(this.grad.degree),
        branch: new FormControl(this.grad.branch),
        joinDate: new FormControl(this.grad.joinDate),
        joinLoc: new FormControl(this.grad.joinLoc),
        feedback: new FormControl(this.grad.feedback),
        skills: new FormControl(this.grad.skills)
      });
      }



    });  
  }

  ngOnInit(): void {
  }

  onSubmit() {
    console.log(this.addGradForm.value);
    this.ugs.updateGrad(this.addGradForm.value).subscribe((resp) => {
      console.log(resp);
    });

        alert('Grad edited!');
        this.r.navigateByUrl('/dashboard');
  }
}
