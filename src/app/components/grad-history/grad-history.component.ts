import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { GradHistoryService } from '../../services/gradHistory/grad-history.service';
import { DetailedGradService } from '../../services/detailedGrad/detailed-grad.service';
import { Grad } from 'src/app/models/Grad';

import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-grad-history',
  templateUrl: './grad-history.component.html',
  styleUrls: ['./grad-history.component.css']
})
export class GradHistoryComponent implements OnInit 
{
  public historyFor: Grad;
  public grads;
  private id: number;

  public columnsToDisplay = ['id', 'updateDate', 'lastUpdateBy','name', 'gender', 'email', 'contact', 'address', 'town', 'college', 'eduSummary'];

  constructor(
    private ghs: GradHistoryService, 
    private ar: ActivatedRoute, 
    public r: Router, 
    private dgs: DetailedGradService) 
  {
    this.ar.params.subscribe(params => {this.id = params.id});
  }

  ngOnInit(): void {
    this.ghs.getGradHistory(this.id).subscribe(resp => {
      this.grads = new MatTableDataSource(resp.body.responseObject);
    });
  }

}
