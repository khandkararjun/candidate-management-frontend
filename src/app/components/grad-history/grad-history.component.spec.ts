import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradHistoryComponent } from './grad-history.component';

describe('GradHistoryComponent', () => {
  let component: GradHistoryComponent;
  let fixture: ComponentFixture<GradHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
