import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validator } from '@angular/forms';
import { AuthService, SocialUser, GoogleLoginProvider } from 'angularx-social-login';
import { LoginService } from '../../services/login.service';
import { User } from '../../models/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private fb:FormBuilder, private authService:AuthService, private ls: LoginService, private r: Router) { }

  ngOnInit(): void {
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData) => {
      // dev
      console.log(userData);
      this.ls.addUser(userData).subscribe((resp : any) => {

        if(resp != null) {
          console.log("Verified user " + resp.email);
          localStorage.setItem('idToken', userData.idToken);
          localStorage.setItem('verifiedUser', resp.email);
          this.r.navigateByUrl('/dashboard');
        } else {
          console.log("Failed to verify user.");
        }

      });
    });
  }

  signOut(): void {
    localStorage.removeItem('idToken');
    localStorage.removeItem('verifiedUser');
    this.authService.signOut();
  }
}
