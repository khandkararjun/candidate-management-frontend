import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { Router } from '@angular/router';

import { SkillsService } from '../../services/skills/skills-service.service';
import { AddGradService } from '../../services/addGrad/add-grad.service';
import { Grad } from 'src/app/models/Grad';

import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-add-grad',
  templateUrl: './add-grad.component.html',
  styleUrls: ['./add-grad.component.css']
})
export class AddGradComponent implements OnInit 
{
  public skills;

  constructor(private ags: AddGradService, private ss: SkillsService, public dialog: MatDialog, private r: Router) {
    ss.getSkills().subscribe((resp) => {
      this.skills = resp.body.responseObject;
    });
  }

  ngOnInit(): void {
  }

  addGradForm = new FormGroup({
    id: new FormControl(''),
    creator: new FormControl(localStorage.getItem('verifiedUser')),
    lastUpdateBy: new FormControl(localStorage.getItem('verifiedUser')),
    createDate: new FormControl(''),
    updateDate: new FormControl(''),
    fname: new FormControl(''),
    mname: new FormControl(''),
    lname: new FormControl(''),
    gender: new FormControl(''),
    email: new FormControl(''),
    contact: new FormControl(''),
    addressLn1: new FormControl(),
    town: new FormControl(''),
    state: new FormControl(''),
    pinCode: new FormControl(''),
    institute: new FormControl(''),
    degree: new FormControl(''),
    branch: new FormControl(''),
    joinDate: new FormControl(''),
    joinLoc: new FormControl(''),
    feedback: new FormControl(''),
    skills: new FormControl('')
  });
/*
  confirmDialog() {
    const dialogRef = this.dialog.open(FormSubmitConfirmDialog);

  }
*/
  onSubmit() {
    console.log("ADDGRAD called" + this.addGradForm.value);

    let grad = new Grad();

    grad.id = this.addGradForm.value.id;
    grad.creator = this.addGradForm.value.creator;
    grad.lastUpdateBy = this.addGradForm.value.lastUpdateBy;
    grad.createDate = this.addGradForm.value.createDate;
    grad.updateDate = this.addGradForm.value.updateDate;
    grad.fname = this.addGradForm.value.fname;
    grad.mname = this.addGradForm.value.mname;
    grad.lname = this.addGradForm.value.lname;
    grad.gender = this.addGradForm.value.gender;
    grad.email = this.addGradForm.value.email;
    grad.contact = this.addGradForm.value.contact;
    grad.addressLn1 = this.addGradForm.value.addressLn1;
    grad.town = this.addGradForm.value.town;
    grad.state = this.addGradForm.value.state;
    grad.pinCode = this.addGradForm.value.pinCode;
    grad.institute = this.addGradForm.value.institute;
    grad.degree = this.addGradForm.value.degree;
    grad.branch = this.addGradForm.value.branch;
    grad.joinDate = this.addGradForm.value.joinDate;
    grad.joinLoc = this.addGradForm.value.joinLoc;
    grad.feedback = this.addGradForm.value.feedback;
    grad.skills = this.addGradForm.value.skills;

    console.log(grad);

    this.ags.addGrad(grad).subscribe((resp) => {
      if(resp.status == "OK") {
        console.log("SUCCESS");
      }
        alert('Grad added!');
        this.r.navigateByUrl('/dashboard');
    });
  }
}
/*
@Component({
  selector: 'form-submit-confirm-dialog',
  templateUrl: 'form-submit-confirm-dialog.html'
})
export class FormSubmitConfirmDialog {}*/