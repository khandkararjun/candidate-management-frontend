import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GradsComponent } from './components/grads/grads.component';
import { LoginComponent } from './components/login/login.component';
import { NoAuthComponent } from './components/no-auth/no-auth.component';
import { TrendsComponent } from './components/trends/trends.component';
import { AddGradComponent } from './components/add-grad/add-grad.component';
import { EditGradComponent } from './components/edit-grad/edit-grad.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GradHistoryComponent } from './components/grad-history/grad-history.component';
import { DetailedGradComponent } from './components/detailed-grad/detailed-grad.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

import { GuardService as Guard } from './services/guard.service';

const routes: Routes = [ {
    path : '',
    component: LoginComponent
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [Guard]
  }, {
    path: 'grads',
    component: GradsComponent,
    canActivate: [Guard]
  }, {
    path: 'grad/:id',
    component: DetailedGradComponent,
    canActivate: [Guard]
  }, {
    path: 'add-grad',
    component: AddGradComponent,
    canActivate: [Guard]
  }, {
    path: 'edit-grad/:id',
    component: EditGradComponent,
    canActivate: [Guard]
  }, {
    path: 'grad/:id/history',
    component: GradHistoryComponent,
    canActivate: [Guard]
  }, {
    path: 'trends',
    component: TrendsComponent,
    canActivate: [Guard]
  }, {
    path: 'no-auth',
    component: NoAuthComponent,
    canActivate: [Guard]
  }, {
    path: '**',
    component: PageNotFoundComponent 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
