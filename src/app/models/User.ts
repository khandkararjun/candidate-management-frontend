export class User {
    email: string;
    token: string;
    name: string;

    constructor(email: string, token: string, name: string) {
        this.email = email;
        this.token = token;
        this.name = name;
    }
}