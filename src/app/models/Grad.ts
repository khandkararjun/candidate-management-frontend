import { Skill } from './Skill';

export class Grad {
    id: number;
    creator: string;
    lastUpdateBy: string;
    createDate: Date;
    updateDate: Date;
    fname: string;
    mname: string;
    lname: string;
    gender: string;
    email: string;
    contact: string;
    addressLn1: string;
    town: string;
    state: string;
    pinCode: string;
    institute: string;
    degree: string;
    branch: string;
    joinDate: string;
    joinLoc: string;
    feedback: string;
    skills: [Skill];
}